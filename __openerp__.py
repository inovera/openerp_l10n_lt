##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 by UAB Inovera (Ltd.) <http://www.inovera.lt>
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name' : 'Lithuania - Chart of Accounts',
    'version' : '1.0',
    'author' : 'UAB Inovera (Ltd.)',
    'category': 'Localization/Account Charts',
    'website': 'http://www.inovera.lt',
    'depends' : ['base', 'base_vat', 'base_iban', 'account',
                                                'account_chart'],
    'init_xml' : [],
    'update_xml' : [
            'account_financial_report.xml',
            'account_type.xml',
            'account_chart_lt.xml',
            'account_tax_code.xml',
            'account_chart_template.xml',
            'account_tax.xml',
            'account_fiscal_position.xml',
            'l10n_chart_lt_wizard.xml',
        ],
    'demo_xml' : [],
    'installable': True
}
